'use strict';

module.exports = class User {
  constructor () {
    this.friends = [];
    this.trips = [];
  }

  getFriends () {
    return this.friends;
  }

  addFriend (user) {
    this.friends.push(user);
  }

  addTrip (trip) {
    this.trips.push(trip);
  }

  trips () {
    return trips;
  }
};