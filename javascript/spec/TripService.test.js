"use strict";

const TripService = require('../src/TripService');

describe('TripService', () => {

    it('should not fail', () => {
        // Given
        const tripService = new TripService();

        // When
        const trips = tripService.getTripsByUser();

        // Then
        expect(trips).toBe('not an exception');
    });

});
